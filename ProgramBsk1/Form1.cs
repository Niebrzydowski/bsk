﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProgramBsk1
{
    public partial class Form1 : Form
    {
        
        RailFence rf;
        Vigenere V;
        PrzestawieniaMacierzowe2a p;
        PrzestawieniaMacierzowe2b p2b;
        /// <summary>
        /// Konstruktor okna Form1. W konstruktorze również wykonuje się wyszukiwanie plików tekstowych i dodawanie ich scieżek do ListBox1.
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            String[] pliki = Directory.GetFiles("Files", "*.txt");
            for (int i = 0; i < pliki.Length; i++)
            {
                ListBox1.Items.Add(pliki[i]);
            }
        }
        /// <summary>
        /// Metoda obsługująca przycisk służący do zyfrowania tekstu. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Zaszyfruj_Click(object sender, EventArgs e)
        {
            Odszyfruj.Enabled = true;
            rf = new RailFence(Convert.ToInt32(Klucz.Value));
            TekstZaszyforwany.Text = rf.Encrypt(TekstSzyforwany.Text);
            
        }
        /// <summary>
        /// Metoda obsługująca przycisk do załadowania ciągu znaków z pliku tekstowego, który jest wybrany w ListBox1.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Wybierz_Click(object sender, EventArgs e)
        {
            if (ListBox1.SelectedIndex != -1)
            {
                string[] linie = System.IO.File.ReadAllLines(ListBox1.SelectedItem.ToString());
                foreach (string linia in linie)
                {
                    if(TabControl1.SelectedIndex==0)
                    TekstSzyforwany.Text = linia;
                   if(TabControl1.SelectedIndex==1)
                    tekstSzyfrowany2a.Text = linia;
                   if (TabControl1.SelectedIndex == 2)
                       TekstSzyfrowany2b.Text = linia;
                   
                }
                if (TekstSzyforwany.Text != null)
                    Zaszyfruj.Enabled = true;
            }
            else
               MessageBox.Show("Nie wybrano pliku z listy!");
        }
        /// <summary>
        /// Metoda obsługująca przycisk do zmiany katalogu z plikami tekstowymi.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZmienKatalog_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                ListBox1.Items.Clear();
                String[] pliki = Directory.GetFiles(dialog.SelectedPath, "*.txt");
                for (int i = 0; i < pliki.Length; i++)
                {
                    ListBox1.Items.Add(pliki[i]);
                }
            }
        }
        /// <summary>
        /// Metoda obsługująca przycisk służący do odszyfrowania tekstu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Odszyfruj_Click(object sender, EventArgs e)
        {
            rf = new RailFence(Convert.ToInt32(Klucz.Value));
            TekstZaszyforwany.Text = rf.Decrypt(TekstZaszyforwany.Text);
            Odszyfruj.Enabled = false;
        }

        private void ZaszyfrujV_Click(object sender, EventArgs e)
        {
            OdszyfrujV.Enabled = true;
            V = new Vigenere(KluczV.Text);
            TekstZaszyfrowanyV.Text = V.Encrypt(TekstSzyfrowanyV.Text);
        }

        private void OdszyfrujV_Click(object sender, EventArgs e)
        {
            V = new Vigenere(KluczV.Text);
            TekstZaszyfrowanyV.Text = V.Decrypt(TekstZaszyfrowanyV.Text);
            OdszyfrujV.Enabled = false;
        }

        private void Zaszyfruj2b_Click(object sender, EventArgs e)
        {

        }

        private void Odszyfruj2b_Click(object sender, EventArgs e)
        {
           
        }

        private void Zaszyfruj2a_Click(object sender, EventArgs e)
        {

            p = new PrzestawieniaMacierzowe2a();
            tekstZaszyfrowany2a.Text = p.Encrypt(Klucz2a.Text, tekstSzyfrowany2a.Text);
           
            
        }

        private void Odszyfruj2a_Click(object sender, EventArgs e)
        {
            p = new PrzestawieniaMacierzowe2a();
            tekstZaszyfrowany2a.Text = p.Decrypyt(Klucz2a.Text, tekstZaszyfrowany2a.Text);
        }

        private void ZaszyfrujB_Click(object sender, EventArgs e)
        {
            p2b = new PrzestawieniaMacierzowe2b();
            tekstZaszyfrowanyB.Text = p2b.Encrypt(Klucz2b.Text,TekstSzyfrowany2b.Text);
        }

        private void OdszyfrujB_Click(object sender, EventArgs e)
        {
            p2b = new PrzestawieniaMacierzowe2b();
            tekstZaszyfrowanyB.Text = p2b.Decrypyt(Klucz2b.Text, tekstZaszyfrowanyB.Text);
        }
       
    }
}
