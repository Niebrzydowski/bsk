﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramBsk1
{
    /// <summary>
    /// Klasa odpowiadająca za przestawienie macierzowe 2a.
    /// </summary>
   public class PrzestawieniaMacierzowe2a
    {
/// <summary>
/// Metoda parsująca klucz z postaci string na jednowymiarową tablicę int.
/// </summary>
/// <param name="key"></param>
/// <returns></returns> 
       private static int[] parseKey(String key)
        {
            string[] temp = key.Split('-');
            int[] keyNumbers = new int[temp.Length];
 
            for (int i = 0; i < temp.Length; i++)
            {
                try
                {
                    keyNumbers[i] = Convert.ToInt32(temp[i]);
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
 
            return keyNumbers;
        }
 /// <summary>
 /// Metoda odowiadająca za szyfrowanie frazy.
 /// </summary>
 /// <param name="key"></param>
 /// <param name="plainText"></param>
 /// <returns></returns> zaszyfrowany tekst
        public  String Encrypt(String key, String plainText)
        {
            int[] klucz = parseKey(key);
            char[,] matrix;
            int tabcount = (plainText.Length + klucz.Length - 1) / klucz.Length;
            matrix = new char[tabcount, klucz.Length];
            for (int i = 0; i < tabcount; i++)
            {
                for (int j = 0; j < klucz.Length; j++)
                    matrix[i, j] = ' ';
            }
            int licznik2 = 0;
            for (int i = 0; i < tabcount; i++)
            {
                for (int j = 0; j < klucz.Length; j++)
                {
                    if (plainText.Length > licznik2)
                    {
                        matrix[i, j] = plainText[licznik2];
                        ++licznik2;
                    }
                }

            }
            
            string wynik = null;
            for (int j = 0; j < tabcount; j++)
            {
                for (int i = 0; i < klucz.Length; i++)
                    wynik += matrix[j, klucz[i] - 1];

            }
            return wynik;
        }
 /// <summary>
 /// Metoda odpowiadająca za odszyfrowanie frazy.
 /// </summary>
 /// <param name="key"></param>
 /// <param name="wynik"></param>
 /// <returns></returns> odszyfrowany tekst
        public  string Decrypyt(string key, string wynik)
        {
            int[] klucz = parseKey(key);
            int tabcount2 = (wynik.Length + klucz.Length - 1) / klucz.Length;

            char[,] matrix2;
            matrix2 = new char[tabcount2, klucz.Length];
            for (int i = 0; i < tabcount2; i++)
            {
                for (int j = 0; j < klucz.Length; j++)
                    matrix2[i, j] = ' ';
            }
            for (int i = 0; i < tabcount2; i++)
            {
                for (int j = 0; j < klucz.Length; j++)
                {
                    if (i * klucz.Length + j >= wynik.Length)
                        break;
                    if (i == tabcount2 - 1 && klucz[j] - 1 >= wynik.Length - i * klucz.Length)
                    {
                        int tempj = j + 1;
                        while (klucz[j] - 1 > wynik.Length - i * klucz.Length && tempj < klucz.Length)
                            tempj++;
                        matrix2[i, klucz[tempj] - 1] = wynik[i * klucz.Length + j];
                    }
                    else
                        matrix2[i, klucz[j] - 1] = wynik[i * klucz.Length + j];
                }

            }
            char[] result = new char[wynik.Length];
            for (int i = 0; i < tabcount2; i++)
            {
                for (int j = 0; j < klucz.Length; j++)
                {
                    try
                    {
                        result[i * klucz.Length + j] = matrix2[i, j];
                    }
                    catch (Exception) { }
                }
            }
 
            return new String(result);
        }
    }
}
