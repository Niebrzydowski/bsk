﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramBsk1
{
    /// <summary>
    /// Klasa odpowiadająca za przestawienie macierzowe 2a.
    /// </summary>
    class PrzestawieniaMacierzowe2b
    {
        /// <summary>
        /// Metoda parsująca klucz z postaci string na jednowymiarową tablicę int.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns> 
        private static int[] parseKey(String key)
        {
            int[] result = new int[key.Length];
            int counter = 0;

            for (char k = 'A'; k < 'Z'; k++)
            {
                for (int i = 0; i < key.Length; i++)
                {
                    if (key[i] == k)
                    {
                        result[i] = counter;
                        counter++;
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// Metoda odpowiadająca za szyfrowanie frazy.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="plainText"></param>
        /// <returns></returns> zaszyfrowany tekst
        public String Encrypt(String key, String plainText)
        {
            int[] klucz = parseKey(key);
            for (int i = 0; i < klucz.Length; i++) Console.Write(klucz[i]);
            Console.WriteLine();
                plainText = plainText.Replace(" ", String.Empty);
               
            int LiczbaWiersza = (plainText.Length + klucz.Length - 1) / klucz.Length;
            char[,] matrix = new char[LiczbaWiersza, klucz.Length];

            int licznik2 = 0;
            for (int i = 0; i <= LiczbaWiersza; i++)
            {
                for (int j = 0; j < klucz.Length; j++)
                {
                    if (plainText.Length > licznik2)
                    {
                        matrix[i, j] = plainText[licznik2];
                        ++licznik2;
                    }
                }
            }
            
            char[] result = new char[2 * plainText.Length + klucz.Length];
            int counter = 0;
            for (int i = 0; i < klucz.Length; i++)
            {
                int keyCounter = -1;
                for (int k = 0; k < klucz.Length; k++)
                {
                    if (klucz[k] == i)
                    {
                        keyCounter = k;
                    }

                    
                }
                
                for (int j = 0; j < LiczbaWiersza; j++)
                {
                    try
                    {
                        if (matrix[j, keyCounter] == '\0')
                        {
                            break;
                        }
                        result[counter++] = matrix[j, keyCounter];
                    }
                    catch (Exception) { }
                }
            }
            return new String(result);
        }
        /// <summary>
        /// Metoda odpowiadająca za odszyfrowanie frazy.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="wynik"></param>
        /// <returns></returns> odszyfrowany tekst
        public string Decrypyt(string key, string wynik)
        {
            
            int[] klucz = parseKey(key);
            int tabsCount = (wynik.Length + klucz.Length - 1) / klucz.Length;
            char[,] tabs = new char[tabsCount, klucz.Length];
            int lastLineCount = wynik.Length - (tabsCount - 1) * klucz.Length;

            char[] result = new char[2 * wynik.Length + klucz.Length];
            int counter = 0;

            for (int i = 0; i < klucz.Length; i++)
            {
                int keyCounter = -1;
                for (int k = 0; k < klucz.Length; k++)
                {
                    if (klucz[k] == i)
                    {
                        keyCounter = k;
                        break;
                    }
                }
                for (int j = 0; j < tabsCount; j++)
                {
                    if (j == tabsCount - 1 && keyCounter >= lastLineCount)
                    {
                        break;
                    }
                    try
                    {
                        tabs[j, keyCounter] = wynik[counter];
                        counter++;
                    }
                    catch (Exception) { }
                }

            }
            counter = 0;

            for (int i = 0; i < tabsCount; i++)
            {
                for (int j = 0; j < klucz.Length; j++)
                {
                    if (tabs[i, j] == '\0')
                        break;
                    result[counter++] = tabs[i, j];
                }
            }

            return new String(result);
        }
    }
}
