﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramBsk1
{
    /// <summary>
    /// Klasa odpowiadająca za szyfr Rail Fence.
    /// </summary>
    class RailFence
    {
        int klucz;
       /// <summary>
       /// Konstruktor
       /// </summary>
       /// <param name="key"></param>
        public RailFence(int key)
        {
            this.klucz = key;
        }
        /// <summary>
        /// Metoda odpowiedzialna za szyfrowanie.
        /// </summary>
        /// <param name="tekst_szyfrowany"></param>
        /// <returns></returns> zaszyfrowany tekst 
        public string Encrypt(string tekst_szyfrowany)
        {
            List<string> railFence = new List<string>();
            for (int i = 0; i < klucz; i++)
            {
                railFence.Add("");
            }

            int numer = 0;
            int przyrost = 1;
            foreach (char c in tekst_szyfrowany)
            {
                if (numer + przyrost == klucz)
                {
                    przyrost = -1;
                }
                else if (numer + przyrost == -1)
                {
                    przyrost = 1;
                }
                railFence[numer] += c;
                numer += przyrost;
            }

            string buffer = "";
            foreach (string s in railFence)
            {
                buffer += s;
            }
            return buffer;
        }
        /// <summary>
        /// Metoda odpowiedzialna za odszyfrowanie. 
        /// </summary>
        /// <param name="tekst_zaszyfrowany"></param>
        /// <returns></returns> odszyfrowany tekst
        public string Decrypt(string tekst_zaszyfrowany)
        {
            int dl_zaszyfrowanego = tekst_zaszyfrowany.Length;
            List<List<int>> railFence = new List<List<int>>();
            for (int i = 0; i < klucz; i++)
            {
                railFence.Add(new List<int>());
            }

            int numer = 0;
            int przyrost = 1;
            for (int i = 0; i < dl_zaszyfrowanego; i++)
            {
                if (numer + przyrost == klucz)
                {
                    przyrost = -1;
                }
                else if (numer + przyrost == -1)
                {
                    przyrost = 1;
                }
                railFence[numer].Add(i);
                numer += przyrost;
            }

            int licznik = 0;
            char[] buffer = new char[dl_zaszyfrowanego];
            for (int i = 0; i < klucz; i++)
            {
                for (int j = 0; j < railFence[i].Count; j++)
                {
                    buffer[railFence[i][j]] = tekst_zaszyfrowany[licznik];
                    licznik++;
                }
            }

            return new string(buffer);
        }
    }
}
