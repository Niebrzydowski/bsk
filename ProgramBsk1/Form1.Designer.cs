﻿namespace ProgramBsk1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.ZmienKatalog = new System.Windows.Forms.Button();
            this.Wybierz = new System.Windows.Forms.Button();
            this.ListBox1 = new System.Windows.Forms.ListBox();
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage1 = new System.Windows.Forms.TabPage();
            this.Odszyfruj = new System.Windows.Forms.Button();
            this.Zaszyfruj = new System.Windows.Forms.Button();
            this.TekstZaszyforwany = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Klucz = new System.Windows.Forms.NumericUpDown();
            this.Label2 = new System.Windows.Forms.Label();
            this.TekstSzyforwany = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.TabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.OdszyfrujV = new System.Windows.Forms.Button();
            this.ZaszyfrujV = new System.Windows.Forms.Button();
            this.TekstZaszyfrowanyV = new System.Windows.Forms.TextBox();
            this.KluczV = new System.Windows.Forms.TextBox();
            this.TekstSzyfrowanyV = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.Odszyfruj2a = new System.Windows.Forms.Button();
            this.Zaszyfruj2a = new System.Windows.Forms.Button();
            this.tekstZaszyfrowany2a = new System.Windows.Forms.TextBox();
            this.Klucz2a = new System.Windows.Forms.TextBox();
            this.tekstSzyfrowany2a = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.OdszyfrujB = new System.Windows.Forms.Button();
            this.ZaszyfrujB = new System.Windows.Forms.Button();
            this.tekstZaszyfrowanyB = new System.Windows.Forms.TextBox();
            this.Klucz2b = new System.Windows.Forms.TextBox();
            this.TekstSzyfrowany2b = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer1)).BeginInit();
            this.SplitContainer1.Panel1.SuspendLayout();
            this.SplitContainer1.Panel2.SuspendLayout();
            this.SplitContainer1.SuspendLayout();
            this.TabControl1.SuspendLayout();
            this.TabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Klucz)).BeginInit();
            this.TabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.SuspendLayout();
            // 
            // SplitContainer1
            // 
            this.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer1.Name = "SplitContainer1";
            // 
            // SplitContainer1.Panel1
            // 
            this.SplitContainer1.Panel1.Controls.Add(this.ZmienKatalog);
            this.SplitContainer1.Panel1.Controls.Add(this.Wybierz);
            this.SplitContainer1.Panel1.Controls.Add(this.ListBox1);
            // 
            // SplitContainer1.Panel2
            // 
            this.SplitContainer1.Panel2.Controls.Add(this.TabControl1);
            this.SplitContainer1.Size = new System.Drawing.Size(636, 438);
            this.SplitContainer1.SplitterDistance = 234;
            this.SplitContainer1.TabIndex = 1;
            // 
            // ZmienKatalog
            // 
            this.ZmienKatalog.Location = new System.Drawing.Point(46, 392);
            this.ZmienKatalog.Name = "ZmienKatalog";
            this.ZmienKatalog.Size = new System.Drawing.Size(144, 23);
            this.ZmienKatalog.TabIndex = 2;
            this.ZmienKatalog.Text = "Zmień katalog";
            this.ZmienKatalog.UseVisualStyleBackColor = true;
            this.ZmienKatalog.Click += new System.EventHandler(this.ZmienKatalog_Click);
            // 
            // Wybierz
            // 
            this.Wybierz.Location = new System.Drawing.Point(46, 341);
            this.Wybierz.Name = "Wybierz";
            this.Wybierz.Size = new System.Drawing.Size(144, 23);
            this.Wybierz.TabIndex = 1;
            this.Wybierz.Text = "Wybierz";
            this.Wybierz.UseVisualStyleBackColor = true;
            this.Wybierz.Click += new System.EventHandler(this.Wybierz_Click);
            // 
            // ListBox1
            // 
            this.ListBox1.FormattingEnabled = true;
            this.ListBox1.HorizontalScrollbar = true;
            this.ListBox1.Location = new System.Drawing.Point(3, 3);
            this.ListBox1.Name = "ListBox1";
            this.ListBox1.Size = new System.Drawing.Size(228, 316);
            this.ListBox1.TabIndex = 0;
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.TabPage1);
            this.TabControl1.Controls.Add(this.TabPage2);
            this.TabControl1.Controls.Add(this.tabPage3);
            this.TabControl1.Controls.Add(this.tabPage4);
            this.TabControl1.Controls.Add(this.tabPage5);
            this.TabControl1.Controls.Add(this.tabPage6);
            this.TabControl1.Location = new System.Drawing.Point(3, 3);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(393, 434);
            this.TabControl1.TabIndex = 0;
            // 
            // TabPage1
            // 
            this.TabPage1.Controls.Add(this.Odszyfruj);
            this.TabPage1.Controls.Add(this.Zaszyfruj);
            this.TabPage1.Controls.Add(this.TekstZaszyforwany);
            this.TabPage1.Controls.Add(this.Label3);
            this.TabPage1.Controls.Add(this.Klucz);
            this.TabPage1.Controls.Add(this.Label2);
            this.TabPage1.Controls.Add(this.TekstSzyforwany);
            this.TabPage1.Controls.Add(this.Label1);
            this.TabPage1.Location = new System.Drawing.Point(4, 22);
            this.TabPage1.Name = "TabPage1";
            this.TabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage1.Size = new System.Drawing.Size(385, 408);
            this.TabPage1.TabIndex = 0;
            this.TabPage1.Text = "Rail Fence";
            this.TabPage1.UseVisualStyleBackColor = true;
            // 
            // Odszyfruj
            // 
            this.Odszyfruj.Enabled = false;
            this.Odszyfruj.Location = new System.Drawing.Point(259, 260);
            this.Odszyfruj.Name = "Odszyfruj";
            this.Odszyfruj.Size = new System.Drawing.Size(101, 23);
            this.Odszyfruj.TabIndex = 7;
            this.Odszyfruj.Text = "Odszyfruj";
            this.Odszyfruj.UseVisualStyleBackColor = true;
            this.Odszyfruj.Click += new System.EventHandler(this.Odszyfruj_Click);
            // 
            // Zaszyfruj
            // 
            this.Zaszyfruj.Enabled = false;
            this.Zaszyfruj.Location = new System.Drawing.Point(26, 260);
            this.Zaszyfruj.Name = "Zaszyfruj";
            this.Zaszyfruj.Size = new System.Drawing.Size(100, 23);
            this.Zaszyfruj.TabIndex = 6;
            this.Zaszyfruj.Text = "Zaszyfruj";
            this.Zaszyfruj.UseVisualStyleBackColor = true;
            this.Zaszyfruj.Click += new System.EventHandler(this.Zaszyfruj_Click);
            // 
            // TekstZaszyforwany
            // 
            this.TekstZaszyforwany.Location = new System.Drawing.Point(26, 196);
            this.TekstZaszyforwany.Name = "TekstZaszyforwany";
            this.TekstZaszyforwany.Size = new System.Drawing.Size(334, 20);
            this.TekstZaszyforwany.TabIndex = 5;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label3.Location = new System.Drawing.Point(23, 167);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(127, 17);
            this.Label3.TabIndex = 4;
            this.Label3.Text = "Fraza zaszyforwana:";
            // 
            // Klucz
            // 
            this.Klucz.Location = new System.Drawing.Point(23, 125);
            this.Klucz.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.Klucz.Name = "Klucz";
            this.Klucz.Size = new System.Drawing.Size(337, 20);
            this.Klucz.TabIndex = 3;
            this.Klucz.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label2.Location = new System.Drawing.Point(20, 98);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(43, 17);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "Klucz:";
            // 
            // TekstSzyforwany
            // 
            this.TekstSzyforwany.Location = new System.Drawing.Point(23, 50);
            this.TekstSzyforwany.Name = "TekstSzyforwany";
            this.TekstSzyforwany.Size = new System.Drawing.Size(337, 20);
            this.TekstSzyforwany.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Label1.Location = new System.Drawing.Point(20, 25);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(114, 17);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Fraza szyfrowana:";
            // 
            // TabPage2
            // 
            this.TabPage2.Controls.Add(this.label9);
            this.TabPage2.Controls.Add(this.label8);
            this.TabPage2.Controls.Add(this.label7);
            this.TabPage2.Controls.Add(this.Odszyfruj2a);
            this.TabPage2.Controls.Add(this.Zaszyfruj2a);
            this.TabPage2.Controls.Add(this.tekstZaszyfrowany2a);
            this.TabPage2.Controls.Add(this.Klucz2a);
            this.TabPage2.Controls.Add(this.tekstSzyfrowany2a);
            this.TabPage2.Location = new System.Drawing.Point(4, 22);
            this.TabPage2.Name = "TabPage2";
            this.TabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage2.Size = new System.Drawing.Size(385, 408);
            this.TabPage2.TabIndex = 1;
            this.TabPage2.Text = "MacierzoweA";
            this.TabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.OdszyfrujB);
            this.tabPage3.Controls.Add(this.ZaszyfrujB);
            this.tabPage3.Controls.Add(this.tekstZaszyfrowanyB);
            this.tabPage3.Controls.Add(this.Klucz2b);
            this.tabPage3.Controls.Add(this.TekstSzyfrowany2b);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(385, 408);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "MacierzoweB";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.button1);
            this.tabPage4.Controls.Add(this.button2);
            this.tabPage4.Controls.Add(this.textBox1);
            this.tabPage4.Controls.Add(this.textBox2);
            this.tabPage4.Controls.Add(this.textBox3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(385, 408);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "MacierzoweC";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.label19);
            this.tabPage5.Controls.Add(this.textBox7);
            this.tabPage5.Controls.Add(this.label16);
            this.tabPage5.Controls.Add(this.label17);
            this.tabPage5.Controls.Add(this.label18);
            this.tabPage5.Controls.Add(this.button3);
            this.tabPage5.Controls.Add(this.button4);
            this.tabPage5.Controls.Add(this.textBox4);
            this.tabPage5.Controls.Add(this.textBox5);
            this.tabPage5.Controls.Add(this.textBox6);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(385, 408);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Caesar";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.OdszyfrujV);
            this.tabPage6.Controls.Add(this.ZaszyfrujV);
            this.tabPage6.Controls.Add(this.TekstZaszyfrowanyV);
            this.tabPage6.Controls.Add(this.KluczV);
            this.tabPage6.Controls.Add(this.TekstSzyfrowanyV);
            this.tabPage6.Controls.Add(this.label6);
            this.tabPage6.Controls.Add(this.label5);
            this.tabPage6.Controls.Add(this.label4);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(385, 408);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Vigenere";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // OdszyfrujV
            // 
            this.OdszyfrujV.Location = new System.Drawing.Point(228, 250);
            this.OdszyfrujV.Name = "OdszyfrujV";
            this.OdszyfrujV.Size = new System.Drawing.Size(126, 23);
            this.OdszyfrujV.TabIndex = 10;
            this.OdszyfrujV.Text = "Odszyfruj";
            this.OdszyfrujV.UseVisualStyleBackColor = true;
            this.OdszyfrujV.Click += new System.EventHandler(this.OdszyfrujV_Click);
            // 
            // ZaszyfrujV
            // 
            this.ZaszyfrujV.Location = new System.Drawing.Point(28, 250);
            this.ZaszyfrujV.Name = "ZaszyfrujV";
            this.ZaszyfrujV.Size = new System.Drawing.Size(124, 23);
            this.ZaszyfrujV.TabIndex = 9;
            this.ZaszyfrujV.Text = "Zaszyfruj";
            this.ZaszyfrujV.UseVisualStyleBackColor = true;
            this.ZaszyfrujV.Click += new System.EventHandler(this.ZaszyfrujV_Click);
            // 
            // TekstZaszyfrowanyV
            // 
            this.TekstZaszyfrowanyV.Location = new System.Drawing.Point(28, 203);
            this.TekstZaszyfrowanyV.Name = "TekstZaszyfrowanyV";
            this.TekstZaszyfrowanyV.Size = new System.Drawing.Size(326, 20);
            this.TekstZaszyfrowanyV.TabIndex = 8;
            // 
            // KluczV
            // 
            this.KluczV.Location = new System.Drawing.Point(28, 131);
            this.KluczV.Name = "KluczV";
            this.KluczV.Size = new System.Drawing.Size(326, 20);
            this.KluczV.TabIndex = 7;
            // 
            // TekstSzyfrowanyV
            // 
            this.TekstSzyfrowanyV.Location = new System.Drawing.Point(28, 57);
            this.TekstSzyfrowanyV.Name = "TekstSzyfrowanyV";
            this.TekstSzyfrowanyV.Size = new System.Drawing.Size(326, 20);
            this.TekstSzyfrowanyV.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(25, 170);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(127, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Fraza zaszyforwana:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(25, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 17);
            this.label5.TabIndex = 3;
            this.label5.Text = "Klucz:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(25, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 17);
            this.label4.TabIndex = 1;
            this.label4.Text = "Fraza szyfrowana:";
            // 
            // Odszyfruj2a
            // 
            this.Odszyfruj2a.Location = new System.Drawing.Point(228, 246);
            this.Odszyfruj2a.Name = "Odszyfruj2a";
            this.Odszyfruj2a.Size = new System.Drawing.Size(129, 23);
            this.Odszyfruj2a.TabIndex = 9;
            this.Odszyfruj2a.Text = "Odszyfruj";
            this.Odszyfruj2a.UseVisualStyleBackColor = true;
            this.Odszyfruj2a.Click += new System.EventHandler(this.Odszyfruj2a_Click);
            // 
            // Zaszyfruj2a
            // 
            this.Zaszyfruj2a.Location = new System.Drawing.Point(25, 246);
            this.Zaszyfruj2a.Name = "Zaszyfruj2a";
            this.Zaszyfruj2a.Size = new System.Drawing.Size(130, 23);
            this.Zaszyfruj2a.TabIndex = 8;
            this.Zaszyfruj2a.Text = "Zaszyfruj";
            this.Zaszyfruj2a.UseVisualStyleBackColor = true;
            this.Zaszyfruj2a.Click += new System.EventHandler(this.Zaszyfruj2a_Click);
            // 
            // tekstZaszyfrowany2a
            // 
            this.tekstZaszyfrowany2a.Location = new System.Drawing.Point(25, 196);
            this.tekstZaszyfrowany2a.Name = "tekstZaszyfrowany2a";
            this.tekstZaszyfrowany2a.Size = new System.Drawing.Size(332, 20);
            this.tekstZaszyfrowany2a.TabIndex = 7;
            // 
            // Klucz2a
            // 
            this.Klucz2a.Location = new System.Drawing.Point(25, 122);
            this.Klucz2a.Name = "Klucz2a";
            this.Klucz2a.Size = new System.Drawing.Size(332, 20);
            this.Klucz2a.TabIndex = 6;
            // 
            // tekstSzyfrowany2a
            // 
            this.tekstSzyfrowany2a.Location = new System.Drawing.Point(25, 52);
            this.tekstSzyfrowany2a.Name = "tekstSzyfrowany2a";
            this.tekstSzyfrowany2a.Size = new System.Drawing.Size(332, 20);
            this.tekstSzyfrowany2a.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(22, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(114, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "Fraza szyfrowana:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(22, 91);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 17);
            this.label8.TabIndex = 11;
            this.label8.Text = "Klucz:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(22, 163);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 17);
            this.label9.TabIndex = 12;
            this.label9.Text = "Fraza zaszyforwana:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(21, 162);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(127, 17);
            this.label10.TabIndex = 20;
            this.label10.Text = "Fraza zaszyforwana:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(21, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 17);
            this.label11.TabIndex = 19;
            this.label11.Text = "Klucz:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(21, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 17);
            this.label12.TabIndex = 18;
            this.label12.Text = "Fraza szyfrowana:";
            // 
            // OdszyfrujB
            // 
            this.OdszyfrujB.Location = new System.Drawing.Point(227, 245);
            this.OdszyfrujB.Name = "OdszyfrujB";
            this.OdszyfrujB.Size = new System.Drawing.Size(129, 23);
            this.OdszyfrujB.TabIndex = 17;
            this.OdszyfrujB.Text = "Odszyfruj";
            this.OdszyfrujB.UseVisualStyleBackColor = true;
            this.OdszyfrujB.Click += new System.EventHandler(this.OdszyfrujB_Click);
            // 
            // ZaszyfrujB
            // 
            this.ZaszyfrujB.Location = new System.Drawing.Point(24, 245);
            this.ZaszyfrujB.Name = "ZaszyfrujB";
            this.ZaszyfrujB.Size = new System.Drawing.Size(130, 23);
            this.ZaszyfrujB.TabIndex = 16;
            this.ZaszyfrujB.Text = "Zaszyfruj";
            this.ZaszyfrujB.UseVisualStyleBackColor = true;
            this.ZaszyfrujB.Click += new System.EventHandler(this.ZaszyfrujB_Click);
            // 
            // tekstZaszyfrowanyB
            // 
            this.tekstZaszyfrowanyB.Location = new System.Drawing.Point(24, 196);
            this.tekstZaszyfrowanyB.Name = "tekstZaszyfrowanyB";
            this.tekstZaszyfrowanyB.Size = new System.Drawing.Size(332, 20);
            this.tekstZaszyfrowanyB.TabIndex = 15;
            // 
            // Klucz2b
            // 
            this.Klucz2b.Location = new System.Drawing.Point(24, 121);
            this.Klucz2b.Name = "Klucz2b";
            this.Klucz2b.Size = new System.Drawing.Size(332, 20);
            this.Klucz2b.TabIndex = 14;
            // 
            // TekstSzyfrowany2b
            // 
            this.TekstSzyfrowany2b.Location = new System.Drawing.Point(24, 51);
            this.TekstSzyfrowany2b.Name = "TekstSzyfrowany2b";
            this.TekstSzyfrowany2b.Size = new System.Drawing.Size(332, 20);
            this.TekstSzyfrowany2b.TabIndex = 13;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(22, 159);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(127, 17);
            this.label13.TabIndex = 28;
            this.label13.Text = "Fraza zaszyforwana:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(22, 87);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(43, 17);
            this.label14.TabIndex = 27;
            this.label14.Text = "Klucz:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label15.Location = new System.Drawing.Point(22, 19);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(114, 17);
            this.label15.TabIndex = 26;
            this.label15.Text = "Fraza szyfrowana:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(228, 242);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(129, 23);
            this.button1.TabIndex = 25;
            this.button1.Text = "Odszyfruj";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(25, 242);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(130, 23);
            this.button2.TabIndex = 24;
            this.button2.Text = "Zaszyfruj";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(25, 193);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(332, 20);
            this.textBox1.TabIndex = 23;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(25, 118);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(332, 20);
            this.textBox2.TabIndex = 22;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(25, 48);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(332, 20);
            this.textBox3.TabIndex = 21;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.Location = new System.Drawing.Point(14, 162);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(127, 17);
            this.label16.TabIndex = 28;
            this.label16.Text = "Fraza zaszyforwana:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label17.Location = new System.Drawing.Point(14, 90);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(66, 17);
            this.label17.TabIndex = 27;
            this.label17.Text = "Klucz nr1:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label18.Location = new System.Drawing.Point(14, 22);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(114, 17);
            this.label18.TabIndex = 26;
            this.label18.Text = "Fraza szyfrowana:";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(220, 245);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(129, 23);
            this.button3.TabIndex = 25;
            this.button3.Text = "Odszyfruj";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(17, 245);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(130, 23);
            this.button4.TabIndex = 24;
            this.button4.Text = "Zaszyfruj";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(17, 196);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(332, 20);
            this.textBox4.TabIndex = 23;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(17, 121);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(130, 20);
            this.textBox5.TabIndex = 22;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(17, 51);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(332, 20);
            this.textBox6.TabIndex = 21;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 10.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label19.Location = new System.Drawing.Point(217, 90);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(66, 17);
            this.label19.TabIndex = 30;
            this.label19.Text = "Klucz nr2:";
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(220, 121);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(129, 20);
            this.textBox7.TabIndex = 29;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(636, 438);
            this.Controls.Add(this.SplitContainer1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.SplitContainer1.Panel1.ResumeLayout(false);
            this.SplitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer1)).EndInit();
            this.SplitContainer1.ResumeLayout(false);
            this.TabControl1.ResumeLayout(false);
            this.TabPage1.ResumeLayout(false);
            this.TabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Klucz)).EndInit();
            this.TabPage2.ResumeLayout(false);
            this.TabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.SplitContainer SplitContainer1;
        internal System.Windows.Forms.Button ZmienKatalog;
        internal System.Windows.Forms.Button Wybierz;
        internal System.Windows.Forms.ListBox ListBox1;
        internal System.Windows.Forms.TabControl TabControl1;
        internal System.Windows.Forms.TabPage TabPage1;
        internal System.Windows.Forms.Button Odszyfruj;
        internal System.Windows.Forms.Button Zaszyfruj;
        internal System.Windows.Forms.TextBox TekstZaszyforwany;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.NumericUpDown Klucz;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox TekstSzyforwany;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.TabPage TabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Button OdszyfrujV;
        private System.Windows.Forms.Button ZaszyfrujV;
        private System.Windows.Forms.TextBox TekstZaszyfrowanyV;
        private System.Windows.Forms.TextBox KluczV;
        private System.Windows.Forms.TextBox TekstSzyfrowanyV;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label label5;
        internal System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button Odszyfruj2a;
        private System.Windows.Forms.Button Zaszyfruj2a;
        private System.Windows.Forms.TextBox tekstZaszyfrowany2a;
        private System.Windows.Forms.TextBox Klucz2a;
        private System.Windows.Forms.TextBox tekstSzyfrowany2a;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button OdszyfrujB;
        private System.Windows.Forms.Button ZaszyfrujB;
        private System.Windows.Forms.TextBox tekstZaszyfrowanyB;
        private System.Windows.Forms.TextBox Klucz2b;
        private System.Windows.Forms.TextBox TekstSzyfrowany2b;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        internal System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBox7;
        internal System.Windows.Forms.Label label16;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
    }
}

