﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProgramBsk1
{
    /// <summary>
    /// Klasa odpowiadająca za szfrowanie Vigenere'a
    /// </summary>
    class Vigenere
    {
        string klucz;
        static char[,] tablica = fillArray();
        /// <summary>
        /// konstruktor
        /// </summary>
        /// <param name="key"></param>
        public Vigenere(string key)
        {
            this.klucz = key;
        }
        /// <summary>
        /// Metoda tworząca tablice
        /// </summary>
        /// <returns></returns>
        private static char[,] fillArray()
        {
            String alfabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            tablica = new char[alfabet.Length, alfabet.Length];

            for (int i = 0; i < alfabet.Length; i++)
            {
                for (int j = 0; j < alfabet.Length; j++)
                {
                    int literka = j + i;

                    if (literka > (alfabet.Length - 1)) 
                    { literka -= alfabet.Length; }
                    tablica[i, j] = Convert.ToChar(alfabet.Substring(literka, 1));
                }
            }

            return tablica;
        }
        /// <summary>
        /// Metoda odpowiadająca za szyfrowanie frazy
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public string Encrypt(string plainText)
        {
            String result = "";

            // usuniecie spacji i zamiana na wielkie litery klucza i tekstu szyfrowanego
            klucz = klucz.Trim().ToUpper();
            plainText = plainText.Trim().ToUpper();

            int keyIndex = 0;
            

            if (klucz.Length <= 0) 
            { return result; }

            for (int i = 0; i < plainText.Length; i++)
            {
                    keyIndex = keyIndex % klucz.Length;
                    result += LookupLetter(klucz.Substring(keyIndex, 1), plainText.Substring(i, 1));
                    keyIndex++;
                
            }
            return result;
        }
        /// <summary>
        /// Metoda zamieniająca znak na zaszyfrowany
        /// </summary>
        /// <param name="znak1"></param>
        /// <param name="znak2"></param>
        /// <returns></returns>
        private static char LookupLetter(String znak1, String znak2)
        {
            int litera1 = Convert.ToInt32(Convert.ToChar(znak1));
            int litera2 = Convert.ToInt32(Convert.ToChar(znak2));

            litera1 -= 65;
            litera2 -= 65;

            if ((litera1 >= 0) && (litera1 <= 26))
            {
                if ((litera2 >= 0) && (litera2 <= 26))
                {
                    return tablica[litera1, litera2];
                }
            }
            return ' ';
        }
        /// <summary>
        /// Metoda odpowiadająca za odszyfrowanie frazy
        /// </summary>
        /// <param name="wynik"></param>
        /// <returns></returns>
        public string Decrypt(string wynik)
        {
            String result = "";

            klucz = klucz.Trim().ToUpper(); ;
            wynik = wynik.Trim().ToUpper();

            int keyIndex = 0;

            if (klucz.Length <= 0) { return result; }

            for (int i = 0; i < wynik.Length; i++)
            {

                    keyIndex = keyIndex % klucz.Length;
                    int keyChar = Convert.ToInt32(Convert.ToChar(klucz.Substring(keyIndex, 1))) - 65;


                    for (int find = 0; find < 26; find++)
                    {
                        if (tablica[keyChar, find].ToString().CompareTo(wynik.Substring(i, 1)) == 0)
                        {
                            result += (char)(find + 65);
                        }
                    }

                    keyIndex++;
                
            }
            return result;
        }



    }
}
